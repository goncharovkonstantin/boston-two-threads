package com.sample;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private Timer timer;
    private List<String> stringNumbers = new ArrayList<>();
    private NumberTranslator numberTranslator;
    private final Integer MONITOR = -1;
    private final int DELAY = 5000;

    public static void main(String[] args) {
        new Main().run();
    }

    public Main()
    {
        numberTranslator = new NumberTranslator();
        timer = new Timer(DELAY, (e) -> System.out.println(getMinimal()));
    }

    public void run()
    {
        System.out.println("Begin typing...");
        Scanner in = new Scanner(System.in);

        while (true)
        {
            String number = in.nextLine();
            if (numberTranslator.translate(number) > 0)
                insertNumber(number);
            if (!(timer.isRunning()))
                timer.start();
        }
    }

    public String getMinimal()
    {
        synchronized (MONITOR)
        {
            if (stringNumbers.size()==0)
                return "No data in memory";

            Integer minimum = null;
            int minIndex = 0;
            for (String number : stringNumbers)
            {
                int currentNumber = numberTranslator.translate(number);
                if (minimum == null || currentNumber < minimum) {
                    minimum = currentNumber;
                    minIndex = getMinIndexOf(number);
                }
            }
            removeNumber(minIndex);
            return minimum.toString();
        }
    }

    public void insertNumber(String number)
    {
        synchronized (MONITOR)
        {
            stringNumbers.add(number);
        }
    }

    public void removeNumber(int index)
    {
        synchronized (MONITOR)
        {
            stringNumbers.remove(index);
        }
    }

    public int getMinIndexOf(String number)
    {
        synchronized (MONITOR)
        {
            return stringNumbers.indexOf(number);
        }
    }
}
