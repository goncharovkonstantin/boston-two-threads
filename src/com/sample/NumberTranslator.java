package com.sample;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NumberTranslator {

    private Map<String, Integer> defaultValues = new HashMap<>();

    public NumberTranslator()
    {
        defaultValues.put("one", 1);
        defaultValues.put("two", 2);
        defaultValues.put("three", 3);
        defaultValues.put("four", 4);
        defaultValues.put("five", 5);
        defaultValues.put("six", 6);
        defaultValues.put("seven", 7);
        defaultValues.put("eight", 8);
        defaultValues.put("nine", 9);
        defaultValues.put("ten", 10);
        defaultValues.put("eleven", 11);
        defaultValues.put("twelve", 12);
        defaultValues.put("thirteen", 13);
        defaultValues.put("fourteen", 14);
        defaultValues.put("fifteen", 15);
        defaultValues.put("sixteen", 16);
        defaultValues.put("seventeen", 17);
        defaultValues.put("eighteen", 18);
        defaultValues.put("nineteen", 19);
        defaultValues.put("twenty", 20);
        defaultValues.put("thirty", 30);
        defaultValues.put("forty", 40);
        defaultValues.put("fifty", 50);
        defaultValues.put("sixty", 60);
        defaultValues.put("seventy", 70);
        defaultValues.put("eighty", 80);
        defaultValues.put("ninety", 90);
        defaultValues.put("hundred",100);
        defaultValues.put("thousand", 1000);
    }

    public int translate(String numberString)
    {
        if (defaultValues.get(numberString) != null)
        {
            return defaultValues.get(numberString);
        }
        List<String> tokens = Arrays.asList(numberString.split(" "));
        int tokensLength = tokens.size();
try{
        switch(tokensLength)
        {
            case (2):
                if (tokens.contains("thousand"))
                    return defaultValues.get(tokens.get(0)) * 1000;
                if (tokens.contains("hundred"))
                    return defaultValues.get(tokens.get(0)) * 100;
                return (defaultValues.get(tokens.get(0)) + defaultValues.get(tokens.get(1)));
            case (3):
                if (tokens.contains("hundred"))
                    return (defaultValues.get(tokens.get(0)) * 100 + defaultValues.get(tokens.get(2)));
                return (defaultValues.get(tokens.get(0)) * 1000 + defaultValues.get(tokens.get(2)));
            case(4):
                if (tokens.contains("hundred"))
                    return (defaultValues.get(tokens.get(0)) * 100 + translate(String.join(" ", tokens.subList(2, tokensLength))));
                return (defaultValues.get(tokens.get(0))*1000 + translate(String.join(" ",tokens.subList(2,tokensLength))));
            case (5):
                return (defaultValues.get(tokens.get(0))*1000 + translate(String.join(" ",tokens.subList(2, tokensLength))));
            case (6):
                return (defaultValues.get(tokens.get(0))*1000 + translate(String.join(" ", tokens.subList(2,tokensLength))));
            default:
                return -1;
        }}
        catch(Exception ex)
        {
            System.out.println("Invalid Format!");
            return -1;
        }
    }
}
